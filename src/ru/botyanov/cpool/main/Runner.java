package ru.botyanov.cpool.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import ru.botyanov.cpool.entity.ConnectionPool;
import ru.botyanov.cpool.entity.ProxyConnection;
import ru.botyanov.cpool.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Runner {
    public static final Logger LOG = Logger.getLogger(Runner.class);
    private static final String LOG_PATH = "config/log4j.xml";
    private static final String SELECT_STUDENTS= "SELECT * FROM users";
    private static final String ADD_USER = "INSERT INTO users (login, password, email) VALUES (?, ?, ?);";

    static {
        new DOMConfigurator().doConfigure(LOG_PATH, LogManager.getLoggerRepository());
    }

    public static void main(String[] args) {
        ConnectionPool connectionPool = null;
        ProxyConnection pConnection = null;
        ArrayList<User> users = new ArrayList<>();
        try {
            connectionPool = ConnectionPool.getInstance();
            pConnection = connectionPool.getConnection();
            //TODO:preparedStatement
            Statement statement = pConnection.createStatement();
            ResultSet result = statement.executeQuery(SELECT_STUDENTS);
            while (result.next()) {
                int id = result.getInt(1);
                int points = result.getInt(2);
                User user = new User(id, points);
                users.add(user);
                LOG.info(user);
            }
        } catch (SQLException e) {
            LOG.error(e);
        } catch (InterruptedException e) {
            LOG.error(e);
        } finally {
            if (connectionPool != null) {
                connectionPool.closeConnection(pConnection);
            }
        }


    }
}
