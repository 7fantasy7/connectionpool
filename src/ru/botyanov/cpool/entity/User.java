package ru.botyanov.cpool.entity;

public class User {
    private long id;
    private long points;

    public User(long id, long points) {
        this.id = id;
        this.points = points;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }
}