package ru.botyanov.cpool.entity;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    public static final Logger LOG = Logger.getLogger(ConnectionPool.class);
    private static ConnectionPool instance;
    private static AtomicBoolean bool = new AtomicBoolean(false);
    private static ReentrantLock lock = new ReentrantLock();
    private ArrayBlockingQueue<ProxyConnection> connectionQueue;

    private ConnectionPool(final int poolSize) {
        connectionQueue = new ArrayBlockingQueue<>(poolSize);

        for (int i = 0; i < poolSize; i++) {
            ProxyConnection connection = createConnection();
            connectionQueue.offer(connection);
        }
    }

    public static ConnectionPool getInstance() {
        if(!bool.get()) {
            try {
                lock.lock();
                if (instance == null) {
                    instance = new ConnectionPool(10);
                    bool.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    private static ProxyConnection createConnection(){
        ProxyConnection pConnection = null;
        try {
            Properties prop = new Properties();
            try {
                prop.load(new FileInputStream("properties/config.properties"));
            } catch (IOException e) {
                LOG.error(e);
            }

            String host = prop.getProperty("db.host");
            String dbName = prop.getProperty("db.name");
            String username = prop.getProperty("db.username");
            String password = prop.getProperty("db.password");

            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            Connection connection = DriverManager.getConnection(host+dbName, username, password);
            pConnection = new ProxyConnection(connection);
        } catch (SQLException e) {
            LOG.error(e);
        }
        return pConnection;
    }

    public ProxyConnection getConnection() throws InterruptedException {
        return connectionQueue.take();
    }

    public void closeConnection(ProxyConnection connection) {
        try {
            connectionQueue.put(connection);
        } catch (InterruptedException e) {
            LOG.error(e);
        }
    }

    public void closeAllConnections() {
        connectionQueue.forEach(ProxyConnection::realCloseConnection);
    }
}